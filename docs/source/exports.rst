Exporting the data
==================

TUCAVOC lets you export the data to different formats.

Some exports will require additional metadata that is needed 
for the tool that will analyse the data.


.. automodule:: tucavoc.exports

Available Formats 
-----------------


Excel
^^^^^

This will simply put the amount fraction and the selected uncertainties 
in an Excel file.


EBAS
^^^^

This format supposed to be compatible with the :ref:`EBAS format <EBAS>`.



QAtool
^^^^^^

This format is for @voc@ tool https://voc-qc.nilu.no/ 


.. _additional_data:

Additional data 
---------------

.. automodule:: tucavoc.additional_data
