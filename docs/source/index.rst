.. tucavoc documentation master file, created by
   sphinx-quickstart on Fri Aug 19 09:38:05 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to TUCAVOC !
====================


TUCAVOC: *automated Tool for Uncertainty Calculation for Atmospheric VOC measurements*

If you want to start using our tool, you can 
first have a look to the :ref:`methodology` and the :ref:`manual`
and then install the TUCAVOC-widget following the 
:ref:`installation instructions <installation>` .

The code can be downloaded from our 
`Github repository <https://gitlab.com/empa503/atmospheric-measurements/tucavoc>`_ 

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   methodology
   install
   manual
   groups
   api
   exports
   sources
   parameters_glossary
   equations_glossary 
   about



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
