.. This file is made to explain the expanded uncertainty.


Finally, the overall uncertainty (or the combined standard uncertainty)
is multiplied by a coverage factor :math:`k=2` to provide the expanded uncertainty.
