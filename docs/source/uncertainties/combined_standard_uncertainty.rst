.. This file is made to explain combined standard uncertainty. 


The
combined uncertainty, or overall uncertainty
is calculated using the law of propagation of the uncertainties
(considering that
the standard uncertainties are not correlated)
according to the 
:ref:`Guide to the Expression of Uncertainty in Measurement <UncertaintyGuide>` .
