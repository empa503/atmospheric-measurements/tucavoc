.. This file is made to explain Sampling Volume Accuracy Uncertainty. 



The standard uncertainty due to application of off-line sampling techniques depends on
the technique used. Contributions to the uncertainty common to all off-line techniques (cleaning of
the samplers, storage, adsorption effects, etc.) should be evaluated case-by-case and per
individual component. If not available in literature, a proper validation of the sorbent tubes is
recommended prior to their use in the field to establish the efficiency of adsorption/desorption and
the safe sampling volume at different composition levels and atmospheric conditions.