# TUCAVOC


[![Documentation Status](https://readthedocs.org/projects/tucavoc/badge/?version=latest)](https://tucavoc.readthedocs.io/en/latest/?badge=latest)


Tucavoc is a python program for calculating uncertainties.

Please read our [documentation](https://tucavoc.readthedocs.io/)